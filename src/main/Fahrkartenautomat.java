package main;

import java.util.Scanner;

class Fahrkartenautomat
{
    static Scanner eingabe = new Scanner(System.in);
    
    static String[] fahrkarten = {"Einzelfahrschein AB", 
                                "Einzelfahrschein BC",
                                "Einzelfahrschein ABC",
                                "Kurzstrecke",
                                "Tageskarte ABC",};
    
    static double[] kosten = {2.3,
                                3.5,
                                3.6,
                                1.5,
                                10};
    
    public static void main(String[] args)
    {       
        while (true) {
            double zuZahlen = fahrkartenEingabe();
            double returnMoney = bezahlen(zuZahlen);
            rueckgeldAusgeben(returnMoney);
            if(!frageNachZusaetzlicherKarte()) break;
            System.out.println();
        }
    }

    public static double fahrkartenEingabe() {
    	
        for(int i = 0; i<fahrkarten.length; i++){
            System.out.println((i + 1) + " " + fahrkarten[i] + ": " + kosten[i] + " EUR");
        }
        
        System.out.print("Bitte W�hlen Sie die gew�nschte Fahrkarte: ");
        int gewaehlteKarte = eingabe.nextInt();
        while (gewaehlteKarte < 1 || gewaehlteKarte > kosten.length) {
            System.out.println("Fahrkarte existiert nicht!");
            System.out.println("Bitte geben Sie eine g�ltige Fahrkarte an:");
            gewaehlteKarte = eingabe.nextInt();
        }
        
        double preis = kosten[gewaehlteKarte-1];
        System.out.print("Anzahl der Fahrkarten eingeben: ");
        int anzahlTickets = eingabe.nextInt();
        while (anzahlTickets < 1 || anzahlTickets > 10) {
            System.out.print("Bitte zwischen 1 und 10 Tickets ausw�hlen: ");
            anzahlTickets = eingabe.nextInt();
        }
        return preis * anzahlTickets;
    }
    
    public static double bezahlen(double gesamtKosten) {
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < gesamtKosten)
        {
            System.out.println("Noch zu zahlen:" + (gesamtKosten - eingezahlterGesamtbetrag));
            System.out.println("Eingabe (0.05, 0.1, 0.2, 0.5, 1, 2 EUR)");
            String eingezahlterBetragStr = eingabe.next();
            double eingezahlterBetrag = 0.0;
            eingezahlterBetragStr = eingezahlterBetragStr.replace(',', '.');
            eingezahlterBetrag = Double.parseDouble(eingezahlterBetragStr);
            eingezahlterGesamtbetrag += eingezahlterBetrag;
        }
        return eingezahlterGesamtbetrag - gesamtKosten;
    }
    
    public static boolean frageNachZusaetzlicherKarte() {
        System.out.print("Wollen Sie noch eine Fahrkarte kaufen? (J/N): ");
        while (true) {
            String antwort = String.valueOf(eingabe.next().charAt(0));
            if (antwort.equalsIgnoreCase("j")) {
                return true;
            } else if (antwort.equalsIgnoreCase("n")) {
                return false;
            } else {
                System.out.print("Bitte mit J oder N antworten: ");
            }
        }
    }

    public static void rueckgeldAusgeben(double r�ckgabebetrag) {
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.println("Sie erhalten nun Ihr R�ckgeld von " + r�ckgabebetrag);
    	   
    	   while(true) { 	
               if(r�ckgabebetrag <= 0.09) break;
    		   
    	       for (int i = 0; i < 8; i++)
    	       {
    	          System.out.print(".");
    	          try {
    				Thread.sleep(250);
    			} catch (InterruptedException e) {
    				e.printStackTrace();
    			}
    	       }

    	       System.out.println("(Klirr)");   		     	       
    	       
               if(r�ckgabebetrag >= 2.0)
               {
            	  System.out.println("2 EURO");
    	          r�ckgabebetrag -= 2.0;
    	          continue;
               }
               
               if(r�ckgabebetrag >= 1.0)
               {
            	  System.out.println("1 EURO");
    	          r�ckgabebetrag -= 1.0;
    	          continue;
               }
               if(r�ckgabebetrag >= 0.5)
               {
            	  System.out.println("50 CENT");
    	          r�ckgabebetrag -= 0.5;
    	          continue;
               }
               if(r�ckgabebetrag >= 0.2)
               {
            	  System.out.println("20 CENT");
     	          r�ckgabebetrag -= 0.2;
    	          continue;
               }
               if(r�ckgabebetrag >= 0.1)
               {
            	  System.out.println("10 CENT");
    	          r�ckgabebetrag -= 0.1;
    	          continue;
               }
               if(r�ckgabebetrag >= 0.05)
               {
            	  System.out.println("5 CENT");
     	          r�ckgabebetrag -= 0.05;
    	          continue;
               } 
    	   }
       }
    }
}
